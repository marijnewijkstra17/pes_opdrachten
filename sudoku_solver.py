
class SudokuSolver(object):
    
    def __init__(self, sudoku):
        self.sudoku = sudoku
        self.is_solved = False
        return
    
    def solve(self):

        n_iterate = 0
        while not self.is_solved and n_iterate < 81:
            for row_index in range(0,9):
                for column_index in range(0,9):

                    if self.sudoku[row_index][column_index]:
                        continue

                    possible_numbers = self.compute_possible_numbers(
                        row_index, column_index)
                    
                    if len(possible_numbers) == 1:
                        self.insert_number(
                            row_index, column_index, possible_numbers[0])
                        
            self.check_is_solved()
            n_iterate += 1
            print(f'Iteration{n_iterate}')
            self.print_solution()
        
        return

    def insert_number(self, row_index, column_index, number):
        self.sudoku[row_index].pop(column_index)
        self.sudoku[row_index].insert(column_index, number)
        return

    def compute_column(self, column_index):

        return [row[column_index] for row in self.sudoku]


    def compute_sub_grid_index(self, index):

        sub_index = index // 3
        index_from = sub_index * 3
        index_to = sub_index * 3 + 3

        return (index_from, index_to)

    def check_in_row(self, row_index, number):
        return number in self.sudoku[row_index]

    def check_in_column(self, column_index, number):
        return number in self.compute_column(column_index)

    def check_in_subgrid(self, row_index, column_index, number):

        row_from, row_to = self.compute_sub_grid_index(row_index)
        column_from, column_to = self.compute_sub_grid_index(column_index)

        is_in_sub_rows = [
            number in self.sudoku[sub_row_index][column_from:column_to]
            for sub_row_index in range(row_from, row_to)]
        
        return any(is_in_sub_rows)

    def compute_possible_numbers(self, row_index, column_index):

        possible_numbers = []
        for number in range(1,10):
            if (self.check_in_row(row_index, number)
                    or self.check_in_column(column_index, number)
                    or self.check_in_subgrid(row_index, column_index, number)):
                continue
            possible_numbers.append(number)
        return possible_numbers


    def check_is_solved(self):
        self.is_solved = all([all(row) for row in self.sudoku])
        return

    def print_solution(self):
        for row in self.sudoku:
            print(row)
        print('\n')
        return
